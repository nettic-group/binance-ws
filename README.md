# Binance WebSocket Connection 
This is a simple Java application that demonstrates how to connect to Binance WebSocket APIs using the binance-api-java library.

### Prerequisites
Before running the application, you need to have the following:

- Java (version 17 )
- [A Binance API key and secret](https://www.binance.com/en/support/faq/how-to-create-api-keys-on-binance-360002502072) - you can create them in your Binance account

## Installation
1. Clone this repository:

```bash
git clone git@gitlab.com:nettic-group/binance-ws.git   
```

2. Set environmental variables and add your API key and secret:

```bash
export BINANCE_API_KEY=<your key>
export BINANCE_API_SECRET=<your secret key>
````

3. Build project
```bash
./mwnw clean install
```
4. Run 
```bash
./mwnw spring-boot:run
```

### Build Docker image
1. build jar file
```bash
./mwnw clean install
```
2. Set environmental variables and add your API key and secret:

```bash
export BINANCE_API_KEY=<your key>
export BINANCE_API_SECRET=<your secret key>
````
3. Build image
```bash
 docker build -t binance-ws --build-arg API_KEY=$BINANCE_API_KEY --build-arg API_SECRET=$BINANCE_API_SECRET .
```
4. Run container
```bash
 docker run -p 8080:8080 binance-ws
```

## Endpoints:
- Subscribe
```bash
curl -X POST "localhost:8080/api/subscribe?symbol=btcusdt" 
```
- Unsubscribe
```bash
curl -X POST "localhost:8080/api/unsubscribe?symbol=btcusdt" 
```







