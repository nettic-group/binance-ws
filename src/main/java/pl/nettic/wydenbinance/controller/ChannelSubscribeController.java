package pl.nettic.wydenbinance.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.nettic.wydenbinance.service.ChannelSubscriber;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class ChannelSubscribeController {

    private final ChannelSubscriber channelSubscriberImpl;

    @PostMapping("/subscribe")
    public ResponseEntity<String> subscribe(@RequestParam("symbol") String symbol) {
        return channelSubscriberImpl.subscribe(symbol);
    }

    @PostMapping("/unsubscribe")
    public ResponseEntity<String> unsubscribe(@RequestParam("symbol") String symbol) {
        return channelSubscriberImpl.unsubscribe(symbol);
    }
}