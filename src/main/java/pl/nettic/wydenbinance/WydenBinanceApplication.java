package pl.nettic.wydenbinance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WydenBinanceApplication {

    public static void main(String[] args) {
        SpringApplication.run(WydenBinanceApplication.class, args);



    }

}