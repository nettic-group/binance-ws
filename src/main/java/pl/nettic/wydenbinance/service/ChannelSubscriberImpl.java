package pl.nettic.wydenbinance.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.Closeable;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
@RequiredArgsConstructor
public class ChannelSubscriberImpl implements ChannelSubscriber {

    private final ChannelService channelService;

    private final Map<String, Closeable> channels = new HashMap<>();

    public ResponseEntity<String> subscribe(String symbol) {
        if (isSubscribed(symbol)) {
            String message = String.format("Already subscribed to symbol %s", symbol);
            log.info(message);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message);
        }
        try {
            add(symbol, channelService.subscribe(symbol));
            String message = String.format("WebSocket started, subscribed to symbol %s", symbol);
            log.info(message);
            return ResponseEntity.ok(message);
        } catch (Exception e) {
            String errorMessage = String.format("An error occurred while subscribing to %s from WebSocket client", symbol);
            log.error(errorMessage);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorMessage);
        }
    }

    private boolean isSubscribed(String symbol) {
        return channels.containsKey(symbol);
    }

    public ResponseEntity<String> unsubscribe(String symbol) {
        Closeable channel = channels.get(symbol);
        if (channel == null) {
            String message = String.format("Not Subscribed to symbol: %s", symbol);
            log.info(message);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(message);
        }
        try (channel) {
            String message;
            message = String.format("Unsubscribed %s from WebSocket client", symbol);
            log.info(message);
            return ResponseEntity.ok(message);
        } catch (IOException e) {
            String errorMessage = String.format("An error occurred while unsubscribing: %s from WebSocket client", symbol);
            log.error(errorMessage, e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errorMessage);
        }
    }

    private void add(String symbol, Closeable channel) {
        if (channel != null) {
            channels.put(symbol, channel);
        }
    }

    private boolean remove(String symbol, Closeable channel) {
        return channels.remove(symbol, channel);
    }

}
