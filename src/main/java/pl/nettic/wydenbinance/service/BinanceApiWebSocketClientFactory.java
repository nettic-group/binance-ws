package pl.nettic.wydenbinance.service;

import com.binance.api.client.BinanceApiWebSocketClient;

public interface BinanceApiWebSocketClientFactory {
    BinanceApiWebSocketClient createWebSocket();

}