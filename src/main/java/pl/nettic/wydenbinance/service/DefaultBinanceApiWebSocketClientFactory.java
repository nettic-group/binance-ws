package pl.nettic.wydenbinance.service;

import com.binance.api.client.BinanceApiClientFactory;
import com.binance.api.client.BinanceApiWebSocketClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class DefaultBinanceApiWebSocketClientFactory implements BinanceApiWebSocketClientFactory {

    private final String apiKey;
    private final String secret;

    public DefaultBinanceApiWebSocketClientFactory(@Value("${binance.api.key}") String apiKey,
                                                    @Value("${binance.api.secret}") String secret) {
        this.apiKey = apiKey;
        this.secret = secret;
    }

    @Override
    public BinanceApiWebSocketClient createWebSocket() {
        BinanceApiClientFactory factory = BinanceApiClientFactory.newInstance(apiKey, secret);
        return factory.newWebSocketClient();
    }
}