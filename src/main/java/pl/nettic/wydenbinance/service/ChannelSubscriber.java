package pl.nettic.wydenbinance.service;

import org.springframework.http.ResponseEntity;

public interface ChannelSubscriber {

    ResponseEntity<String> subscribe(String symbol);

    ResponseEntity<String> unsubscribe(String symbol);
}
