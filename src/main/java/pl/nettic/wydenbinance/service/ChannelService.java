package pl.nettic.wydenbinance.service;

import com.binance.api.client.BinanceApiWebSocketClient;
import com.binance.api.client.domain.market.OrderBookEntry;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.nettic.wydenbinance.util.DateTimeUtils;

import java.io.Closeable;
import java.util.List;

@Service
@Slf4j
public class ChannelService {

    private final BinanceApiWebSocketClient client;

    private final DateTimeUtils dateTimeUtils;


    public ChannelService(BinanceApiWebSocketClientFactory webSocketClientFactory, DateTimeUtils dateTimeUtils) {
        client = webSocketClientFactory.createWebSocket();
        this.dateTimeUtils = dateTimeUtils;
    }

    public Closeable subscribe(String symbol) {
        return client.onDepthEvent(symbol, response -> {
            String time = dateTimeUtils.convertTimestampToString(response.getEventTime());
            List<OrderBookEntry> bids = response.getBids();
            List<OrderBookEntry> asks = response.getAsks();
            OrderBookEntry bid = bids.get(0);
            OrderBookEntry ask = asks.get(0);
            log.info("{} - {}, bid: {} bid-qty {}, ask: {} ask-qty: {}", time, symbol.toUpperCase(), bid.getPrice(), bid.getQty(), ask.getPrice(), ask.getQty());
        });
    }
}