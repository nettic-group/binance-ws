package pl.nettic.wydenbinance.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoSettings;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.Closeable;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

@MockitoSettings
class ChannelSubscriberImplTest {

    @Mock
    private ChannelService channelService;

    private ChannelSubscriberImpl channelSubscriber;

    @BeforeEach
    void setUp() {
        channelSubscriber = new ChannelSubscriberImpl(channelService);
    }

    @Test
    void testSubscribeSuccess() {
        String symbol = "BTCUSDT";
        Closeable mockCloseable = mock(Closeable.class);

        doReturn(mockCloseable).when(channelService).subscribe(symbol);

        ResponseEntity<String> responseEntity = channelSubscriber.subscribe(symbol);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test
    void testSubscribeAlreadySubscribed() {
        String symbol = "BTCUSDT";
        Closeable mockCloseable = mock(Closeable.class);

        doReturn(mockCloseable).when(channelService).subscribe(symbol);

        channelSubscriber.subscribe(symbol);

        ResponseEntity<String> responseEntity = channelSubscriber.subscribe(symbol);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
    }


    @Test
    void testUnsubscribeSuccess() throws Exception {
        String symbol = "BTCUSDT";
        Closeable mockCloseable = mock(Closeable.class);

        doReturn(mockCloseable).when(channelService).subscribe(symbol);

        channelSubscriber.subscribe(symbol);

        ResponseEntity<String> responseEntity = channelSubscriber.unsubscribe(symbol);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Test
    void testUnsubscribeNotSubscribed() {
        String symbol = "BTCUSDT";

        ResponseEntity<String> responseEntity = channelSubscriber.unsubscribe(symbol);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
    }
}